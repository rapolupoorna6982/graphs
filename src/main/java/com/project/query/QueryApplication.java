package com.project.query;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart; 
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset; 
import org.jfree.data.category.DefaultCategoryDataset; 
import org.jfree.ui.ApplicationFrame; 
import org.jfree.ui.RefineryUtilities;

import java.io.File;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

import org.springframework.boot.autoconfigure.SpringBootApplication;



@SpringBootApplication
public class QueryApplication  {
	public static void Draw_Chart(DefaultCategoryDataset my_bar_chart_dataset,String W_heading,String heading,String x_name,String y_name)
	{
		 JFreeChart  barChart=ChartFactory.createBarChart(heading,x_name,y_name,my_bar_chart_dataset,PlotOrientation.VERTICAL,true,true,false);             
	     ChartFrame frame = new ChartFrame(W_heading, barChart);
	     frame.pack();
	     frame.setVisible(true);
	}
	public  static void Upds_by_feed_Month(Connection con,String Year,String month,String feed) throws Exception
	{
		    DefaultCategoryDataset my_bar_chart_dataset = new DefaultCategoryDataset();
		    PreparedStatement statement ; 
	        String spl = Year+"-"+month+"-"+"01";
	        statement = con.prepareStatement("SELECT DATEDIFF(DATE_ADD(?, INTERVAL 1 MONTH),?)"); 
	        statement.setString(1,spl);
	        statement.setString(2,spl);
	        ResultSet rs = statement.executeQuery();
	        rs.next();
	        int days = rs.getInt(1);
	        System.out.println(days);
	
	        for(int i=1;i<=days;++i)
	        {
	        	statement = con.prepareStatement("SELECT COUNT(product_id_i) FROM products WHERE DATE(IN_Z)=? and prod_upd_src_c=? ; ");
	            String s=Integer.toString(i);
	            String Date = Year+"-"+month+"-"+s;
	            statement.setString(1,Date);
	            statement.setString(2,feed);
	            rs = statement.executeQuery();
	            rs.next();
	            String user = "Day"+s+"->"+rs.getString(1);
	            my_bar_chart_dataset.addValue(rs.getInt(1),"Feed-"+feed,s);
	            System.out.println(user+"hello");
	           
	        }
	        Draw_Chart(my_bar_chart_dataset,"Results","Updates in a month by feed","Date","No_Of_Upadtes");
	     
	}
	public  static void ActiveFeeds_in1M(Connection con,String Year,String month) throws Exception
	{
		    DefaultCategoryDataset my_bar_chart_dataset = new DefaultCategoryDataset();
		    PreparedStatement statement ; 
	        String spl = Year+"-"+month+"-"+"01";
	        statement = con.prepareStatement("SELECT DATEDIFF(DATE_ADD(?, INTERVAL 1 MONTH),?)"); 
	        statement.setString(1,spl);
	        statement.setString(2,spl);
	        ResultSet rs = statement.executeQuery();
	        rs.next();
	        int days = rs.getInt(1);
	       
	        for(int i=1;i<=days;++i)
	        {
	        	statement = con.prepareStatement("SELECT prod_upd_src_c,COUNT(product_id_i) FROM products WHERE DATE(IN_Z)=? GROUP BY prod_upd_src_c ORDER BY COUNT(product_id_i) desc LIMIT 5; ");
	            String s=Integer.toString(i);
	            String Date = Year+"-"+month+"-"+s;
	            System.out.println(Year+"-"+month+"-"+s);
	            statement.setString(1,Date);
	            rs = statement.executeQuery();
	            int count =0;
	            while(rs.next())
	            {
	            	count++;
	            String user = "Day"+s+"->"+rs.getString(1)+"->"+rs.getString(2);
	            my_bar_chart_dataset.addValue(rs.getInt(2),rs.getString(1),s);
	           
	            }
	           if(count == 0)
	           {
	        	   my_bar_chart_dataset.addValue(0,"",s);
	           }
	        }
	        Draw_Chart(my_bar_chart_dataset,"Results","5 most active feeds in a month","Date","No_Of_Upadtes");
	     
	}
	public static void Upds_by_feed_Day(Connection con,String Date,String feed) throws Exception
	{
		 DefaultCategoryDataset my_bar_chart_dataset = new DefaultCategoryDataset();
		 PreparedStatement statement ; 
		 ResultSet rs;
		 for(int i=0;i<24;++i)
	     {
	        	statement = con.prepareStatement("SELECT COUNT(product_id_i) FROM products WHERE DATE(IN_Z)=? and HOUR(IN_Z)=? and prod_upd_src_c=? ; ");
	            String hour=Integer.toString(i);
	            statement.setString(1,Date);
	            statement.setString(2,hour);
	            statement.setString(3,feed);
	            rs = statement.executeQuery();
	            rs.next();
	            my_bar_chart_dataset.addValue(rs.getInt(1),"ACTIVE USERS",hour);
	           
	     }
		   Draw_Chart(my_bar_chart_dataset,"Results","Updates by a feed on a day","Date","No_Of_Upadtes");
	}
	public static void ActiveFeeds_in_Day(Connection con,String Date,String feed) throws Exception
	{
		 DefaultCategoryDataset my_bar_chart_dataset = new DefaultCategoryDataset();
		 PreparedStatement statement ; 
		 ResultSet rs;
		 for(int i=0;i<24;++i)
	        {
	        	statement = con.prepareStatement("SELECT prod_upd_src_c,COUNT(product_id_i) FROM products WHERE DATE(IN_Z)=? and HOUR(IN_Z)=? GROUP BY prod_upd_src_c ORDER BY COUNT(product_id_i) desc LIMIT 5; ");
	            String s=Integer.toString(i);
	            statement.setString(1,Date);
	            statement.setString(2,s);
	            rs = statement.executeQuery();
	            int count =0;
	            while(rs.next())
	            {
	            	count++;
	            String user = "Day"+s+"->"+rs.getString(1)+"->"+rs.getString(2);
	            my_bar_chart_dataset.addValue(rs.getInt(2),rs.getString(1),s);
	           
	            }
	           if(count == 0)
	           {
	        	   my_bar_chart_dataset.addValue(0,"",s);
	           }
	        }
		 Draw_Chart(my_bar_chart_dataset,"Results","5 most active feeds in a Day","Date","No_Of_Upadtes");
	}
	public static void main(String[] args) throws Exception{
		
		String url = "jdbc:mysql://127.0.0.1:3306/sample";
      	String uname = "root";
    	    String pass = "Siripo@5";
        Class.forName("com.mysql.cj.jdbc.Driver");
        Connection con = DriverManager.getConnection(url,uname,pass);
        System.out.println("success"); 
        PreparedStatement statement ; 
		 ResultSet rs;
		 statement = con.prepareStatement("select * from products");
		 rs = statement.executeQuery();
		 while(rs.next())
         {
            String user = rs.getString(1)+" "+rs.getString(2)+" "+rs.getString(3)+" "+rs.getString(4)+" "+rs.getString(5);
            System.out.println(user);
         }
     	//String Year = "2021";
       // String month = "06";
        //String Date= "2021-06-18";
        //String feed = "feed1";
        /*
        Upds_by_feed_Month(con,Year,month,feed);
       
        Upds_by_feed_Day(con,Date,feed);
       
        ActiveFeeds_in1M(con,Year,month) ;
   
        ActiveFeeds_in_Day(con,Date,feed); 
        */
		 Scanner s = new Scanner(System.in);
		 int a= s.nextInt();
	       
		
	}

}
